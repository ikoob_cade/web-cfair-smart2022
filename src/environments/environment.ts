// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  // base_url: 'https://d3v-server-cf-air-api.ikoob.co.kr/api/v1', // 개발서버
  // base_url: 'http://192.168.1.11:8038/api/v1', // 고진혁 pc
  base_url: 'https://cfair-api.cf-air.com/api/v1', // 운영서버
  socket_url: 'https://cfair-socket-ssl.cf-air.com',
  // eventId: '61ef9ba01b8f6a1487f7a3ff', // smart healthcare 2022 Dev
  // socket_url: 'https://d3v-server-cf-air-socket-api.ikoob.co.kr',

  eventId: '61ef9688d4b8480012fd765a' // smart healthcare 2022 Prod
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
