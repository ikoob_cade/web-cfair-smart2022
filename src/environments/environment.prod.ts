export const environment = {
  production: true,
  base_url: 'https://cfair-api.cf-air.com/api/v1', // 운영서버
  socket_url: 'https://cfair-socket-ssl.cf-air.com',
  eventId: '61ef9688d4b8480012fd765a' // smart healthcare 2022 Prod
};
