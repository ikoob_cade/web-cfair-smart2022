import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { BoardService } from '../../services/api/board.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class BoardComponent implements OnInit {
  public search: FormGroup;
  public curPage = 1;
  public boards: any[] = [];

  public totalCount = 0;
  public totalPagesCount = 0;

  public myPostingsState = false; // 내가쓴 글보기 활성화 값

  public user = sessionStorage.getItem('cfair')
    ? JSON.parse(sessionStorage.getItem('cfair'))
    : null;

  private lastInput = '';

  constructor(
    private fb: FormBuilder,
    private boardService: BoardService,
    private router: Router
  ) {
    this.search = this.fb.group({
      keywordSelect: ['title'],
      keyword: [''],
    });

    /**
     * 컴포넌트 생성 시 상세에서 복귀한 경우
     * 현재 페이지, 자신이 쓴글 활성화 여부를 변경한다.
     */
    const navigation = this.router.getCurrentNavigation();
    if (navigation.extras?.state) {
      const { myPostingsState, curPage, keywordSelect, keyword } =
        navigation.extras.state;
      this.myPostingsState = myPostingsState || false;
      this.curPage = curPage || 1;

      if (keywordSelect) {
        this.search.controls.keywordSelect.setValue(keywordSelect);
      }

      if (keyword) {
        this.search.controls.keyword.setValue(keyword);
      }
    }
  }

  ngOnInit(): void {
    this.getBoards(this.myPostingsState);
  }

  // 게시글 목록 조회
  getBoards(myPostings?): void {
    const params: any = { ...this.search.value };
    params.page = this.curPage;

    if (myPostings) {
      params.myPost = true;
    }

    this.boardService.find(params).subscribe((res) => {
      this.boards = res.boards;
      this.totalCount = res.totalCount;
      this.totalPagesCount = res.totalPagesCount;

      this.myPostingsState = myPostings ? true : false;

      if (this.search.controls.keyword.value) {
        this.lastInput = this.search.controls.keyword.value;
      }
    });
  }

  // 검색
  submit(): void {
    this.curPage = 1;
    this.getBoards(this.myPostingsState);
  }

  // 페이징 이벤트
  changePage(page): void {
    this.curPage = page;
    this.getBoards(this.myPostingsState);
  }

  // 해당 아이템의 상세 페이지로 이동한다.
  goDetail(board, type): void {
    const state = {
      myPostingsState: this.myPostingsState,
      curPage: this.curPage,
      keywordSelect: this.search.controls.keywordSelect.value || '',
      keyword: this.lastInput || '',
      index:
        type === 'post' ? board.index : type === 'notice' ? 'Notice' : 'FAQ',
    };

    this.router.navigateByUrl(`/board/${board.id}`, {
      state,
    });
  }

  // 자신이 쓴글 목록 조회
  getMyPostings(): void {
    this.curPage = 1;
    this.search.controls.keyword.setValue('');
    this.search.controls.keywordSelect.setValue('title');
    this.getBoards(true);
  }

  // 모든 목록 조회
  getAllBoards(): void {
    this.curPage = 1;
    this.search.controls.keyword.setValue('');
    this.search.controls.keywordSelect.setValue('title');
    this.getBoards(false);
  }
}
