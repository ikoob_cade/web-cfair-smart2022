import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): any {
    const auth = JSON.parse(sessionStorage.getItem('cfair'));
    if (auth && auth.token) {
      return true;
    } else {
      // 아래 path는 로그인 상태에서만 접근 가능
      const path = ['live', 'vod', 'vod/:vodId', 'posters', 'posters/:posterId', 'my-page'];

      const result = path.indexOf(route.routeConfig.path);
      if (result > -1) {
        this.router.navigate(['/login']);
      }
      return true;
    }
  }
}

